<?php
declare(strict_types=1);
// SPDX-FileCopyrightText: Marc Schreiber <info@schrieveslaach.de>
// SPDX-License-Identifier: AGPL-3.0-or-later

namespace OCA\OpenDocumentForms\AppInfo;

use OCP\AppFramework\App;

class Application extends App {
	public const APP_ID = 'opendocumentforms';

	public function __construct() {
		parent::__construct(self::APP_ID);
	}
}
