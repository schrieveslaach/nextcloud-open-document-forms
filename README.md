# Open Document Forms

This project is a work in progress with the goal to provide a Nextcloud app that let's you use
forms ([created with LibreOffice](https://books.libreoffice.org/en/WG71/WG7118-Forms.html)) to
submit them as documents.

For example, you could create a ODT document that represents an application, enabling people to
become a member of your association/club. Based on the ODT document the app will generate a public
HTML page that request the user to fill the fields of the ODT form. After filling the form the app
will generate a PDF version of the document and stores it in your Nextcloud instance so that you
are able to process the user's application.

## Development

1. Ensure that you installed the [LibreOffice SDK](https://api.libreoffice.org/docs/install.html)
   and run the script `setsdkenv_unix` to setup required environment variables.
2. Build and start the compiled program with `cargo run`


