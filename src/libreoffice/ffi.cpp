#include <com/sun/star/beans/XPropertySet.hpp>
#include <com/sun/star/bridge/XUnoUrlResolver.hpp>
#include <com/sun/star/container/XNameContainer.hpp>
#include <com/sun/star/drawing/XDrawPagesSupplier.hpp>
#include <com/sun/star/form/XForm.hpp>
#include <com/sun/star/form/XFormsSupplier2.hpp>
#include <com/sun/star/frame/Desktop.hpp>
#include <com/sun/star/frame/XComponentLoader.hpp>
#include <com/sun/star/frame/XStorable.hpp>
#include <com/sun/star/io/SequenceInputStream.hpp>
#include <com/sun/star/io/SequenceOutputStream.hpp>
#include <com/sun/star/lang/XMultiComponentFactory.hpp>
#include <com/sun/star/text/XTextDocument.hpp>
#include <cppuhelper/bootstrap.hxx>
#include <exception>
#include <functional>
#include <libreoffice/com/sun/star/uno/Reference.h>
#include <osl/file.hxx>
#include <osl/process.h>
#include <rtl/bootstrap.hxx>
#include <stdexcept>

#include "open-document-forms/include/libreoffice_ffi.hpp"
#include "open-document-forms/src/libreoffice/mod.rs.h"

using namespace std;
using namespace cppu;
using namespace rtl;
using namespace css::uno;
using namespace css::beans;
using namespace css::bridge;
using namespace css::frame;
using namespace css::lang;
using namespace css::text;
using namespace css::form;
using namespace css::drawing;
using namespace css::container;
using namespace css::io;

TextDocumentHandle::TextDocumentHandle(Reference<XMultiComponentFactory> factory, Reference<XTextDocument> textDocument,
                                       Reference<XComponentContext> componentContext)
    : factory(factory), textDocument(textDocument), componentContext(componentContext) {}

TextDocumentHandle::~TextDocumentHandle() {
   textDocument->dispose();
   componentContext->release();
   Reference<XComponent>::query(factory)->dispose();
}

void TextDocumentHandle::process_form_fields(
    std::function<void(const char *, Reference<XTextRange> &)> textRangeConsumerFunction) const {
   Reference<XDrawPagesSupplier> drawPagesSupplier(textDocument, UNO_QUERY);
   assert(drawPagesSupplier.is() && "Cannot get draw pages supplier");

   auto drawPages = drawPagesSupplier->getDrawPages();
   auto drawPage = drawPages->getByIndex(0);
   Reference<XFormsSupplier2> formsSupplier(drawPage, UNO_QUERY);
   assert(formsSupplier.is() && "Cannot get forms supplier supplier");

   auto forms = formsSupplier->getForms();
   for (auto name : forms->getElementNames()) {
      auto f = forms->getByName(name);
      Reference<XNameContainer> container(f, UNO_QUERY);
      assert(container.is() && "Cannot convert form into name container, should never happen");

      for (auto elemName : container->getElementNames()) {
         auto t = container->getByName(elemName);
         Reference<XTextRange> textRange(t, UNO_QUERY);
         assert(textRange.is() && "Cannot convert form element into name text range");

         textRangeConsumerFunction(elemName.toUtf8().getStr(), textRange);
      }
   }
}

std::unique_ptr<TextDocumentHandle> open_document_handle(rust::Str connectionString, rust::Str offapiRdbUrl,
                                                         const rust::Vec<rust::u8> &bytes) {

   Bootstrap::set("URE_MORE_TYPES", OUString::createFromAscii(std::string(offapiRdbUrl).c_str()));

   Reference<XComponentContext> xComponentContext(::cppu::defaultBootstrap_InitialComponentContext());

   /* Gets the service manager instance to be used (or null). This method has
      been added for convenience, because the service manager is an often used
      object. */
   Reference<XMultiComponentFactory> xMultiComponentFactoryClient(xComponentContext->getServiceManager());

   /* Creates an instance of a component which supports the services specified
      by the factory */
   Reference<XInterface> xInterface =
       xMultiComponentFactoryClient->createInstanceWithContext("com.sun.star.bridge.UnoUrlResolver", xComponentContext);

   Reference<XUnoUrlResolver> resolver(xInterface, UNO_QUERY);

   xInterface = Reference<XInterface>(
       resolver->resolve(OUString::createFromAscii(std::string(connectionString).c_str())), UNO_QUERY);

   // gets the server component context as property of the office component factory
   Reference<XPropertySet> xPropSet(xInterface, UNO_QUERY);
   xPropSet->getPropertyValue("DefaultContext") >>= xComponentContext;

   // gets the service manager from the office
   Reference<XMultiComponentFactory> xMultiComponentFactoryServer(xComponentContext->getServiceManager());

   /* Creates an instance of a component which supports the services specified
      by the factory. Important: using the office component context. */
   Reference<XDesktop2> xComponentLoader = Desktop::create(xComponentContext);

   Sequence<::sal_Int8> s(bytes.size());
   std::copy(bytes.begin(), bytes.end(), s.begin());

   auto readProps = Sequence<PropertyValue>(1);
   readProps[0].Name = "InputStream";
   readProps[0].Value <<= SequenceInputStream::createStreamFromSequence(xComponentContext, s);

   Reference<XComponent> xComponent = xComponentLoader->loadComponentFromURL(
       OUString::createFromAscii("private:stream"), OUString("_blank"), 0, readProps);

   Reference<XTextDocument> textDocument(xComponent, UNO_QUERY);
   if (!textDocument.is()) {
      xComponent->dispose();
      Reference<XComponent>::query(xMultiComponentFactoryClient)->dispose();
      throw std::domain_error("Not a text document");
   }

   return std::unique_ptr<TextDocumentHandle>(
       new TextDocumentHandle(xMultiComponentFactoryClient, textDocument, xComponentContext));
}

rust::Vec<rust::String> TextDocumentHandle::get_form_fields() const {
   rust::Vec<rust::String> fields;

   process_form_fields([&fields](const char *elemName, __attribute__((__unused__)) Reference<XTextRange> textRange) {
      fields.push_back(elemName);
   });

   return fields;
}

void TextDocumentHandle::fill_form_fields(const FormKeyValuePairs &keyValuePairs) const {
   process_form_fields([&keyValuePairs](const char *elemName, Reference<XTextRange> textRange) {
      auto value = OUString::createFromAscii(keyValuePairs.get_string(elemName).c_str());
      textRange->setString(value);
   });
}

rust::Vec<rust::u8> TextDocumentHandle::write_as_pdf() const {
   auto outputStream = SequenceOutputStream::create(componentContext);

   auto filterData = Sequence<PropertyValue>(1);
   filterData[0].Name = "ExportFormFields";
   filterData[0].Value <<= false;

   Reference<XStorable> storable(textDocument, UNO_QUERY);
   auto storeProps = Sequence<PropertyValue>(3);
   storeProps[0].Name = "FilterName";
   storeProps[0].Value <<= OUString("writer_pdf_Export");
   storeProps[1].Name = "OutputStream";
   storeProps[1].Value <<= outputStream;
   storeProps[2].Name = "FilterData";
   storeProps[2].Value <<= filterData;

   storable->storeToURL("private:stream", storeProps);

   rust::Vec<rust::u8> bytes;
   auto sequence = outputStream->getWrittenBytes();
   for (auto it = sequence.begin(); it != sequence.end(); ++it) {
      bytes.push_back(*it);
   }

   return bytes;
}
