mod rocket;

use std::{
    collections::HashMap,
    path::PathBuf,
    process::{Child, Command},
};
use tokio::io::{AsyncRead, AsyncReadExt};
use url::Url;

#[cxx::bridge]
mod ffi {
    extern "Rust" {
        type FormKeyValuePairs;
        fn get_string(self: &FormKeyValuePairs, key: &str) -> String;
    }

    unsafe extern "C++" {

        include!("open-document-forms/include/libreoffice_ffi.hpp");

        type TextDocumentHandle;
        fn get_form_fields(self: &TextDocumentHandle) -> Vec<String>;
        fn fill_form_fields(
            self: &TextDocumentHandle,
            key_value_pairs: &FormKeyValuePairs,
        ) -> Result<()>;
        fn write_as_pdf(self: &TextDocumentHandle) -> Result<Vec<u8>>;

        fn open_document_handle(
            connect_string: &str,
            offapi_rdb_url: &str,
            document_stream: &Vec<u8>,
        ) -> Result<UniquePtr<TextDocumentHandle>>;
    }
}

/// This struct helps to manage a headless instance of LibreOffice that is required to parse text documents.
///
/// Also keep in mind that it might become insecure, see [here](https://stackoverflow.com/a/55104888/5088458) when
/// allowing to upload any OpenDocument.
pub struct HeadlessLibreOfficeProcess {
    command_child: Child,
    port: u16,
    office_program_path: PathBuf,
    temp_user_installation: PathBuf,
}

impl HeadlessLibreOfficeProcess {
    pub fn with_random_port() -> Result<Self, Error> {
        let office_program_path = std::env::var("OFFICE_PROGRAM_PATH")
            .map_err(|_e| Error::MissingOfficeProgramPathVariable)?;
        let office_program_path = PathBuf::from(office_program_path);
        // TODO: generate error if office_program_path is not a directory

        use backoff::ExponentialBackoff;
        use std::net::{IpAddr, Ipv4Addr, SocketAddr, TcpListener, TcpStream};

        let port = {
            let listener = TcpListener::bind("127.0.0.1:0").unwrap();
            listener.local_addr().unwrap().port()
        };

        let temp_user_installation = {
            let mut user_installation = std::env::temp_dir();
            let x = rand::random::<u32>();
            user_installation.push(format!("od-forms-{x}"));
            user_installation
        };

        let command_child = Command::new("soffice")
            .arg(format!(
                "--accept=socket,host=localhost,port={port};urp;StarOffice.ServiceManager"
            ))
            .arg("--headless")
            .arg(format!(
                "-env:UserInstallation={}",
                Url::from_file_path(temp_user_installation.clone()).unwrap()
            ))
            .spawn()
            .expect("TODO: expected to run libreoffice in headless mode");

        let backoff = ExponentialBackoff::default();
        backoff::retry(backoff, || {
            let socket = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), port);
            let _ = TcpStream::connect_timeout(&socket, std::time::Duration::from_secs(2))?;
            Ok(())
        })
        .unwrap();

        Ok(Self {
            command_child,
            port,
            office_program_path,
            temp_user_installation,
        })
    }

    pub async fn open_document_handle<'a, 'b: 'a, R>(
        &'b self,
        read: &mut R,
    ) -> Result<TextDocumentHandle<'a>, Error>
    where
        R: AsyncRead + std::marker::Unpin,
    {
        let mut bytes = Vec::new();
        read.read_to_end(&mut bytes).await.expect("TODO");

        let ffi_handle = ffi::open_document_handle(
            &self.connection_string(),
            self.offapi_rdb_url().as_str(),
            &bytes,
        )?;

        Ok(TextDocumentHandle {
            ffi_handle: unsafe { Box::from_raw(ffi_handle.into_raw()) },
            _libreoffice_process_lifetime: std::marker::PhantomData,
        })
    }

    fn connection_string(&self) -> String {
        format!(
            "uno:socket,host=localhost,port={port};urp;StarOffice.ServiceManager",
            port = self.port
        )
    }

    fn offapi_rdb_url(&self) -> Url {
        let mut offapi_rdb = self.office_program_path.clone();
        offapi_rdb.push("types");
        offapi_rdb.push("offapi.rdb");

        Url::from_file_path(offapi_rdb).unwrap()
    }
}

impl Drop for HeadlessLibreOfficeProcess {
    fn drop(&mut self) {
        use nix::sys::signal::{self, Signal};
        use nix::unistd::Pid;

        signal::kill(
            Pid::from_raw(self.command_child.id() as i32),
            Signal::SIGTERM,
        )
        .expect("TODO: libreoffice should be killed");

        std::fs::remove_dir_all(&self.temp_user_installation)
            .expect("Temp user installation should be deletable");
    }
}

pub struct TextDocumentHandle<'a> {
    ffi_handle: Box<ffi::TextDocumentHandle>,
    _libreoffice_process_lifetime: std::marker::PhantomData<&'a ()>,
}

impl<'a> TextDocumentHandle<'a> {
    pub fn get_form_fields(&self) -> Vec<String> {
        self.ffi_handle.get_form_fields()
    }

    pub fn fill_form_fields<I, S>(&mut self, key_value_pairs: I) -> Result<(), Error>
    where
        I: IntoIterator<Item = (S, S)>,
        S: AsRef<str>,
    {
        Ok(self
            .ffi_handle
            .fill_form_fields(&FormKeyValuePairs::from(key_value_pairs))?)
    }

    pub fn pdf_stream(self) -> PdfStream<'a> {
        PdfStream {
            ffi_handle: self.ffi_handle,
            bytes: None,
            _libreoffice_process_lifetime: std::marker::PhantomData,
        }
    }
}

pub struct FormKeyValuePairs {
    // TODO: HashMap<&'a str, &'a str> is the goal to avoid copies
    key_value_pairs: HashMap<String, String>,
}

impl std::fmt::Debug for TextDocumentHandle<'_> {
    fn fmt(&self, _: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Ok(())
    }
}

impl FormKeyValuePairs {
    pub fn get_string(&self, key: &str) -> String {
        self.key_value_pairs
            .get(key)
            .map(Clone::clone)
            .unwrap_or_else(|| String::from("Hello"))
    }
}

impl<'a, I, S: 'a> std::convert::From<I> for FormKeyValuePairs
where
    I: IntoIterator<Item = (S, S)>,
    S: AsRef<str>,
{
    fn from(iter: I) -> Self {
        let key_value_pairs = iter
            .into_iter()
            .map(|(k, v)| (String::from(k.as_ref()), String::from(v.as_ref())))
            .collect::<HashMap<_, _>>();
        FormKeyValuePairs { key_value_pairs }
    }
}

#[derive(Debug)]
pub enum Error {
    ConnectionRefused,
    NotATextDocument,
    MissingOfficeProgramPathVariable,
}

impl std::convert::From<cxx::Exception> for Error {
    fn from(ex: cxx::Exception) -> Self {
        match ex.what() {
            "Not a text document" => Error::NotATextDocument,
            what if what
                .starts_with("Connector : couldn't connect to socket (Connection refused)") =>
            {
                Error::ConnectionRefused
            }
            what => todo!("Unknown error case: {}", what),
        }
    }
}

impl std::convert::From<std::io::Error> for Error {
    fn from(err: std::io::Error) -> Self {
        match err.kind() {
            // std::io::ErrorKind::NotFound => Error::InvalidPath,
            var => todo!("Unknown failure case for {var}"),
        }
    }
}

pub struct PdfStream<'a> {
    ffi_handle: Box<ffi::TextDocumentHandle>,
    bytes: Option<Vec<u8>>,
    _libreoffice_process_lifetime: std::marker::PhantomData<&'a ()>,
}

/// TODO: This is not yet a real async implementation. Please, change write_as_pdf to handle streaming
impl AsyncRead for PdfStream<'_> {
    fn poll_read(
        mut self: std::pin::Pin<&mut Self>,
        _cx: &mut std::task::Context<'_>,
        buf: &mut tokio::io::ReadBuf<'_>,
    ) -> std::task::Poll<std::io::Result<()>> {
        let mut bytes = match self.bytes.take() {
            None => self.ffi_handle.write_as_pdf().unwrap(),
            Some(bytes) => bytes,
        };

        buf.put_slice(
            &bytes
                .drain(0..std::cmp::min(bytes.len(), buf.remaining()))
                .into_iter()
                .collect::<Vec<_>>()[..],
        );

        self.bytes = Some(bytes);
        std::task::Poll::Ready(Ok(()))
    }
}

// TODO: is there another way????
unsafe impl Send for PdfStream<'_> {}

#[cfg(test)]
mod tests {
    use super::*;
    use tokio::fs::File;

    #[static_init::dynamic(drop)]
    static mut LIBREOFFICE: HeadlessLibreOfficeProcess =
        HeadlessLibreOfficeProcess::with_random_port()
            .expect("Should be able to create a libreoffice process");

    #[tokio::test]
    async fn resolve_input_fields_from_first_name_example() {
        let mut file = File::open("example-docs/firstname.odt").await.unwrap();
        let libreoffice = LIBREOFFICE.read();
        let document_handle = libreoffice.open_document_handle(&mut file).await.unwrap();
        let fields = document_handle.get_form_fields();

        assert_eq!(fields, vec![String::from("First Name")]);
    }

    #[tokio::test]
    async fn fails_to_resolve_form_fields_for_none_text_documents() {
        let mut file = File::open("example-docs/a presentation.odp").await.unwrap();
        let libreoffice = LIBREOFFICE.read();
        let err = libreoffice
            .open_document_handle(&mut file)
            .await
            .unwrap_err();

        assert!(matches!(err, Error::NotATextDocument));
    }

    #[test]
    fn convert_connection_refused_error() {
        let libreoffice = LIBREOFFICE.read();
        let result = ffi::open_document_handle(
            &format!(
                "uno:socket,host=localhost,port={port};urp;StarOffice.ServiceManager",
                port = libreoffice.port + 1
            ),
            libreoffice.offapi_rdb_url().as_str(),
            &Vec::new(),
        );

        match result {
            Ok(_) => assert!(false, "should fail"),
            Err(err) => assert!(matches!(err.into(), Error::ConnectionRefused)),
        }
    }

    #[tokio::test]
    async fn fill_form() {
        use lopdf::Document;
        use pdf_extract::{output_doc, PlainTextOutput};

        let form_value = "Mike";

        let mut file = File::open("example-docs/firstname.odt").await.unwrap();
        let libreoffice = LIBREOFFICE.read();
        let mut document_handle = libreoffice.open_document_handle(&mut file).await.unwrap();
        document_handle
            .fill_form_fields([("First Name", form_value)])
            .unwrap();

        let mut bytes = Vec::new();
        document_handle
            .pdf_stream()
            .read_to_end(&mut bytes)
            .await
            .unwrap();

        let document = Document::load_mem(&bytes).unwrap();

        let mut text = String::new();
        let mut output = PlainTextOutput::new(&mut text);
        output_doc(&document, &mut output).unwrap();
        assert!(
            text.contains("Mike"),
            "PDF Text should contain text “{form_value}”, but was “{text}”",
        );
    }
}
