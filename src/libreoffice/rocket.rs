use crate::libreoffice::{Error, TextDocumentHandle};
use rocket::http::{ContentType, Header};
use rocket::response::{self, stream::ReaderStream, Responder};
use rocket::{Request, Response};

impl<'r> Responder<'r, 'static> for Error {
    fn respond_to(self, _request: &'r rocket::Request<'_>) -> rocket::response::Result<'static> {
        match self {
            Error::ConnectionRefused => todo!(),
            Error::NotATextDocument => todo!(),
            Error::MissingOfficeProgramPathVariable => todo!(),
        }
    }
}

impl<'r> Responder<'r, 'r> for TextDocumentHandle<'r> {
    fn respond_to(self, _: &'r Request<'_>) -> response::Result<'r> {
        let pdf_stream = self.pdf_stream();

        Response::build()
            .header(ContentType::PDF)
            .header(Header::new(
                "Content-Disposition",
                "attachment; filename=\"document.pdf\"",
            ))
            .streamed_body(ReaderStream::one(pdf_stream))
            .ok()
    }
}
