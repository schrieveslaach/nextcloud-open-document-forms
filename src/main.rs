use crate::libreoffice::{Error, HeadlessLibreOfficeProcess, TextDocumentHandle};
use rocket::form::Form;
use rocket::http::ContentType;
use rocket::State;
use std::collections::HashMap;
use tokio::fs::File;
use yew::prelude::*;
use yew::ServerRenderer;

#[macro_use]
extern crate rocket;

mod libreoffice;

struct TextDocmuntForm {}

#[derive(Properties, PartialEq)]
struct TextDocumentFields {
    fields: Vec<String>,
}

impl Component for TextDocmuntForm {
    type Message = ();
    type Properties = TextDocumentFields;

    fn create(_ctx: &Context<Self>) -> Self {
        Self {}
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let props = ctx.props();
        html! {
            <form action="/" method="post">
            {
                props.fields.iter().map(|field| {
                    let encoded_field = urlencoding::encode(field).to_string();
                    html! {<div>
                        <label for={encoded_field.clone()}>{ field }</label>
                        <input type="text" name={encoded_field.clone()} id={encoded_field.clone()}/>
                    </div>}
                }).collect::<Html>()
            }
                <input type="submit" value="Submit"/>
            </form>
        }
    }
}

#[get("/")]
async fn index(
    libreoffice: &State<HeadlessLibreOfficeProcess>,
) -> Result<(ContentType, String), Error> {
    let mut file = File::open("example-docs/firstname.odt").await.unwrap();
    let renderer = {
        let document_handle = libreoffice.open_document_handle(&mut file).await?;
        let form_fields = document_handle.get_form_fields();

        ServerRenderer::<TextDocmuntForm>::with_props(|| {
            yew::props! {
                TextDocumentFields {
                    fields: form_fields
                }
            }
        })
    };

    let html = renderer.render().await;
    Ok((ContentType::HTML, html))
}

#[post("/", data = "<fill_form_data>")]
async fn fill_form<'a, 'r: 'a>(
    fill_form_data: Form<HashMap<String, String>>,
    libreoffice: &'r State<HeadlessLibreOfficeProcess>,
) -> Result<TextDocumentHandle<'a>, Error> {
    let mut file = File::open("example-docs/firstname.odt").await.unwrap();
    let mut document_handle = libreoffice.open_document_handle(&mut file).await?;

    document_handle.fill_form_fields(
        fill_form_data
            .iter()
            .map(|(k, v)| (urlencoding::decode(&k).unwrap().to_string(), v.clone()))
            .collect::<HashMap<String, String>>(),
    )?;

    Ok(document_handle)
}

#[launch]
fn rocket() -> _ {
    let libreoffice = HeadlessLibreOfficeProcess::with_random_port().unwrap();
    rocket::build()
        .mount("/", routes![index, fill_form])
        .manage(libreoffice)
}
