#[macro_use]
extern crate cmd_lib;

use simple_eyre::eyre::{Result, WrapErr};

fn main() -> Result<()> {
    let libreoffice_program_path_variable = std::env::var("OFFICE_PROGRAM_PATH")
        .wrap_err("Environment variable OFFICE_PROGRAM_PATH is required")?;
    let libreoffice_sdk_path_variable =
        std::env::var("OO_SDK_HOME").wrap_err("Enviroment variable OO_SDK_HOME is required")?;
    run_cmd!(
        cppumaker -Gc -O "./target/libreoffice"
            "$libreoffice_program_path_variable/types.rdb"
            "$libreoffice_program_path_variable/types/offapi.rdb";
    )?;

    let mut bridge = cxx_build::bridge("src/libreoffice/mod.rs");
    bridge
        .file("src/libreoffice/ffi.cpp")
        .define("LINUX", None)
        .include("target/libreoffice")
        .include(format!("{libreoffice_sdk_path_variable}/include"))
        .object(format!("{libreoffice_sdk_path_variable}/lib/libuno_sal.so"))
        .object(format!(
            "{libreoffice_sdk_path_variable}/lib/libuno_cppu.so"
        ))
        .object(format!(
            "{libreoffice_sdk_path_variable}/lib/libuno_cppuhelpergcc3.so"
        ));

    for file in std::fs::read_dir(&libreoffice_program_path_variable)? {
        let path = file?.path();

        if let Some(ext) = path.extension() {
            if ext == "so" {
                bridge.object(path);
            }
        }
    }

    let entries = bridge.recorded_compile("libreoffice");

    cc::store_json_compilation_database(&entries, "target/cxxbridge/compile_commands.json");

    Ok(())
}
