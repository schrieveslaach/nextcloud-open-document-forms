#pragma once

#include <algorithm>
#include <com/sun/star/lang/XMultiComponentFactory.hpp>
#include <com/sun/star/text/XTextDocument.hpp>
#include <com/sun/star/uno/Exception.hpp>
#include <cppuhelper/implbase1.hxx>
#include <functional>
#include <libreoffice/rtl/ustring.hxx>
#include <memory>
#include <rtl/bootstrap.hxx>

#include "rust/cxx.h"

struct FormKeyValuePairs;

class TextDocumentHandle {
 public:
   TextDocumentHandle(css::uno::Reference<css::lang::XMultiComponentFactory> factory,
                      css::uno::Reference<css::text::XTextDocument> textDocument,
                      css::uno::Reference<css::uno::XComponentContext> componentContext);
   ~TextDocumentHandle();

   void process_form_fields(
       std::function<void(const char *, css::uno::Reference<css::text::XTextRange> &)> textRangeConsumerFunction) const;

   rust::Vec<rust::String> get_form_fields() const;

   void fill_form_fields(const FormKeyValuePairs &keyValuePairs) const;

   rust::Vec<rust::u8> write_as_pdf() const;

 private:
   css::uno::Reference<css::lang::XMultiComponentFactory> factory;
   css::uno::Reference<css::text::XTextDocument> textDocument;
   css::uno::Reference<css::uno::XComponentContext> componentContext;
};

std::unique_ptr<TextDocumentHandle> open_document_handle(rust::Str connectionString, rust::Str offapiRdbUrl,
                                                         const rust::Vec<rust::u8> &bytes);

namespace rust {
namespace behavior {
template <typename Try, typename Fail> static void trycatch(Try &&func, Fail &&fail) noexcept try {
   func();
} catch (const css::uno::Exception &e) {
   fail(rtl::OUStringToOString(e.Message, RTL_TEXTENCODING_ASCII_US).getStr());
} catch (const std::exception &e) {
   fail(e.what());
}
} // namespace behavior
} // namespace rust
